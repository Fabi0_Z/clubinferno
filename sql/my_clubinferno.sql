-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Giu 29, 2017 alle 13:55
-- Versione del server: 10.1.21-MariaDB
-- Versione PHP: 5.6.30

DROP SCHEMA IF EXISTS my_clubinferno;
CREATE SCHEMA my_clubinferno;
USE my_clubinferno;
SET AUTOCOMMIT=0;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my_clubinferno`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `aggiornamento`
--

CREATE TABLE `aggiornamento` (
  `idAggiornamento` int(11) NOT NULL,
  `titolo` varchar(100) COLLATE latin1_general_cs NOT NULL DEFAULT 'Aggiornamento',
  `testo` text COLLATE latin1_general_cs
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Struttura della tabella `contattosponsor`
--

CREATE TABLE `contattosponsor` (
  `IDdocumento` int(4) NOT NULL,
  `numero` int(20) NOT NULL,
  `intestatario` varchar(50) CHARACTER SET latin1 NOT NULL,
  `fkSponsor` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Struttura della tabella `mailsponsor`
--

CREATE TABLE `mailsponsor` (
  `IDmail` int(4) NOT NULL,
  `indirizzoMail` varchar(50) CHARACTER SET latin1 NOT NULL,
  `fkSponsor` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Struttura della tabella `pagamentosponsor`
--

CREATE TABLE `pagamentosponsor` (
  `idPagamento` int(11) NOT NULL,
  `data` date NOT NULL,
  `anno` year(4) NOT NULL,
  `fkSponsor` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `sedesponsor`
--

CREATE TABLE `sedesponsor` (
  `IDsede` int(4) NOT NULL,
  `indirizzo` varchar(50) CHARACTER SET latin1 NOT NULL,
  `città` varchar(50) CHARACTER SET latin1 NOT NULL,
  `provincia` varchar(50) CHARACTER SET latin1 NOT NULL,
  `fkSponsor` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

-- --------------------------------------------------------

--
-- Struttura della tabella `sponsor`
--

CREATE TABLE `sponsor` (
  `IDsponsor` int(4) NOT NULL,
  `nome` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `descrizione` text COLLATE latin1_general_cs NOT NULL,
  `sito` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `immagineLogo` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `immagine` varchar(50) COLLATE latin1_general_cs NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Dump dei dati per la tabella `sponsor`
--

INSERT INTO `sponsor` (`IDsponsor`, `nome`, `descrizione`, `sito`, `immagineLogo`, `immagine`) VALUES
(1, 'Trachite Artigiana', 'L\'azienda, nata nei primi mesi del 2004, è giovane e flessibile, punta a consolidare la sua posizione sul mercato come una delle principali segherie di trachite. \r\n\r\nPunto di forza dell\'azienda è la coltivazione di più cave per l’estrazione e la lavorazione di 6 tipi differenti di trachite, varietà che permette di poter disporre di grosse quantità di trachite e una varia gamma cromatica. Lavorazioni speciali e costi contenuti rendono la trachite una valida alternativa a pietre come il granito. \r\n\r\nL’azienda si posiziona oggi sul mercato come interlocutrice attenta e desiderosa di offrire, un prodotto di qualità a prezzi sicuramente concorrenziali, uniti ad una seria politica di vendita e preparazione professionale del team aziendale.', 'http://www.trachiteartigiana.com', '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `idUtente` int(4) NOT NULL,
  `nome` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `cognome` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `username` varchar(50) COLLATE latin1_general_cs NOT NULL,
  `dataNascita` date NOT NULL,
  `codiceTessera` int(5) DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `password` varchar(255) COLLATE latin1_general_cs NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`idUtente`, `nome`, `cognome`, `username`, `dataNascita`, `codiceTessera`, `email`, `password`) VALUES
(19, 'Fabio', 'Sussarellu', 'Fabi0_Z', '1998-10-15', NULL, 'sussarellu98@gmail.com', '$2y$10$V7YQyh6GnOIWf1Qz/4zcqevVhbrME70PxjkTLb0IPhRBSfBBm127q'),
(12, 'Giovanni', 'Orani', 'Moriarty', '1996-08-08', NULL, 'heavycliff@gmail.com', '$2y$10$MVaxdTGol63jwhBgL0Po3eIBVAoZJqbAURNOt9vS/NVexwKPKxBb2'),
(13, 'Stefano', 'Leoni', 'Mr. President 80', '1980-12-23', NULL, 'samcro.1980@gmail.com', '$2y$10$jP7txZiP/ANV5J34RM7sres3GnJxL7fQ1bgkesw2rGq7d0YIM34Ki'),
(14, 'Pierfrancesco', 'Delle Carote', 'ciao', '2017-06-18', NULL, 'mipiaceilcazzo666@gmail.com', '$2y$10$eng0Ug5EU7vmecqkVebN/uUr/gFvtN4kunLIgvowmM3ePDxq1biE2'),
(15, 'Nicole', 'Mura', 'Hysteria98', '1998-08-20', NULL, 'nicole.mura2@gmail.com', '$2y$10$zuMS81pwbElmdoluNuxO2ObOHaHHBQpemVqj/0DR3JtwD0vODBKNS'),
(16, 'Nicole', 'Mura', 'Hysteria.98', '1998-08-20', NULL, 'nicole.mura@libero.it', '$2y$10$Q9YtOXsLR3MTrjqcQApZCOxuwLI1JQRVo6zI2O.TpmgZYFTW4JCha');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `aggiornamento`
--
ALTER TABLE `aggiornamento`
  ADD PRIMARY KEY (`idAggiornamento`);
ALTER TABLE `aggiornamento` ADD FULLTEXT KEY `testo` (`testo`);

--
-- Indici per le tabelle `contattosponsor`
--
ALTER TABLE `contattosponsor`
  ADD PRIMARY KEY (`IDdocumento`);

--
-- Indici per le tabelle `mailsponsor`
--
ALTER TABLE `mailsponsor`
  ADD PRIMARY KEY (`IDmail`);

--
-- Indici per le tabelle `pagamentosponsor`
--
ALTER TABLE `pagamentosponsor`
  ADD PRIMARY KEY (`idPagamento`);

--
-- Indici per le tabelle `sedesponsor`
--
ALTER TABLE `sedesponsor`
  ADD PRIMARY KEY (`IDsede`);

--
-- Indici per le tabelle `sponsor`
--
ALTER TABLE `sponsor`
  ADD PRIMARY KEY (`IDsponsor`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`idUtente`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `aggiornamento`
--
ALTER TABLE `aggiornamento`
  MODIFY `idAggiornamento` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `contattosponsor`
--
ALTER TABLE `contattosponsor`
  MODIFY `IDdocumento` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `mailsponsor`
--
ALTER TABLE `mailsponsor`
  MODIFY `IDmail` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `pagamentosponsor`
--
ALTER TABLE `pagamentosponsor`
  MODIFY `idPagamento` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `sedesponsor`
--
ALTER TABLE `sedesponsor`
  MODIFY `IDsede` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `sponsor`
--
ALTER TABLE `sponsor`
  MODIFY `IDsponsor` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `utente`
--
ALTER TABLE `utente`
  MODIFY `idUtente` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
