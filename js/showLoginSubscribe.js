function showLoginSubscribe() {
    var modal = document.getElementById('login');
    var modal2 = document.getElementById('sign-up');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal || event.target == modal2) {
            modal.style.display = "none";
            modal2.style.display = "none";
        }
    }
    // When the user press the esc key, close the modal
    document.addEventListener('keydown', function (event) {
        if (event.keyCode == 27) {
            modal.style.display = "none";
            modal2.style.display = "none";
        }
    });
}