<?php 
require 'config.php';
$registerNome = stripslashes($_POST["name"]);
$registerCognome = stripslashes($_POST["surname"]);
$registerMail = stripslashes($_POST["email"]);
$registerUsername = stripslashes($_POST["username"]);
$registerPassword = stripslashes($_POST["password"]);
$registerDataNascita = $_POST["dataNascita"];

$registerNome = mysqli_real_escape_string($conn, $registerNome);
$registerCognome = mysqli_real_escape_string($conn, $registerCognome);
$registerMail = mysqli_real_escape_string($conn, $registerMail);
$registerUsername = mysqli_real_escape_string($conn, $registerUsername);
$registerPassword = mysqli_real_escape_string($conn, $registerPassword);
$registerDataNascita = mysqli_real_escape_string($conn, $registerDataNascita);

$sql = "SELECT * FROM utente WHERE email='$registerMail' OR username='$registerUsername'";
$resultset = mysqli_query($conn, $sql);
$row = mysqli_fetch_assoc($resultset);

if(!$row['email'] and !$row['username'])
{
    $passwordCriptata = password_hash($registerPassword, PASSWORD_DEFAULT);
	$sql = "INSERT INTO utente(idUtente, nome, cognome, username, dataNascita, CodiceTessera, email, password) 
			VALUES (NULL, '$registerNome', '$registerCognome', '$registerUsername', '$registerDataNascita' , NULL , '$registerMail' , '$passwordCriptata' )";
	if ($conn->query($sql) === TRUE) 
    {
        $end = strtotime('2037-12-31');
        setcookie("username", $registerUsername, $end, "/");
        setcookie("nome", $registerNome, $end, "/");
        setcookie("password", $passwordCriptata, $end, "/");
        header("location: ../home");
        exit();
	} else {
		echo "Error: " . $sql . "<br>" . $conn->error;
	}
} 
else 
{
    session_start();
    if($row["email"] == $registerMail)
    {
        $_SESSION["invalid_register_email"] = true;
    }
    if($row["username"] == $registerUsername)
    {
        $_SESSION["invalid_register_username"] = true;
    }
	header("location: ../");
    exit();
}
$conn->close();
?>