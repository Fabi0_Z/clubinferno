<?php
    unset($_COOKIE['username']);
    unset($_COOKIE['password']);
    setcookie("username", null, -1, "/");
    setcookie("password", null, -1, "/");
    header("Location: ../");
    exit();
?>