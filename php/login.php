<?php
    if(isset($_POST['login']))
    {
        if (empty($_POST['username']) || empty($_POST['password'])) 
        {
            echo "Username or Password is invalid";
        }
        else 
        {
            require'config.php';
            $username = stripslashes($_POST['username']);
            $password = stripslashes($_POST['password']);
            $username = mysqli_real_escape_string($conn,$username);
            $password = mysqli_real_escape_string($conn,$password);
            $sql = "SELECT username, nome, password 
		            FROM utente 
		            WHERE username = '$username' OR codiceTessera = '$username' OR email = '$username';";
            $result = $conn->query($sql);
            if($result->num_rows == 1)
            {
                $row = $result->fetch_assoc();
                if(password_verify($password, $row["password"]))
                {
                    if(isset ($_POST["resta-connesso"]))
                    {
                        $end = strtotime('2037-12-31');
                        setcookie("username", $row["username"], $end, "/");
                        setcookie("nome", $row["nome"], $end, "/");
                        setcookie("password", $row["password"], $end, "/");
                        header("location: ../home");
                        exit();
                    }
                    else {
                        setcookie("username", $username, 0, "/");
                        setcookie("password", $row["password"], 0, "/");
                        header("location: ../home");
                        exit();
                    }
                }
                else 
                {
                    session_start();
                    $_SESSION['invalid_password'] = true;
                    header("location: ../");
                    exit();
                }
            }
            else
            {
                session_start();
                $_SESSION['invalid_user'] = true;
                header("location: ../");
                exit();
            }
            mysqli_close($conn);
        }
    }
?>
